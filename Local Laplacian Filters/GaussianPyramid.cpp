//////////////////////////////////////////////////////////////////////////////////////////////////
//
// Local Laplacian Filters: Edge-aware Image Processing with a Laplacian Pyramid
// For "Praktikum: Bildverarbeitung / Bildkommunikation"
// At the Eberhardt Karls Universität Tübingen, Germany
// 
// This project tries to implement a user friendly GUI to perform image processing
// using the local laplacian filtering method proposed by Paris et al. in their Paper
// "Local Laplacian Filters: Edge-aware Image Processing with a Laplacian Pyramid" (2011).
// The project is using the Gtk+ 3.6.4 GUI Library Bundle for the graphical interface and
// the OpenCV 2.4.9 Open Source Computer Vision Library Bundle for the computation. 
// The code for the HDRImage class is based on Igor Kravtchenko's code (flipcode.com).
//
// Credit goes to Sylvain Paris, Samuel W.Hasinoff, Jan Kautz and Igor Kravtchenko
//
// Author is Adrian Czarkowski
// See me also on Bitbucket, where this project is hosted:
// https://bitbucket.org/kinecticats/local-laplacian-filters-edge-aware-image-processing-with-a
//
//////////////////////////////////////////////////////////////////////////////////////////////////


#include "GaussianPyramid.h"


/**
* Constructor for the gaussian pyramid
* @param image: the input image
* @param limit: the maximum depth
*/
GaussianPyramid::GaussianPyramid(cv::Mat image, int limit)
{
	// Extract the image size
	int height = image.rows;
	int width = image.cols;

	// Create the subwindow for the pyramid
	cv::Vec4i subwindow = { 1, height, 1, width };

	// Store the limit as depth
	levels = limit;

	// Store the input image as first level of the gaussian pyramid
	level.push_back(image);

	// Get the pyramid filter
	cv::Mat filter = get_filter();

	for (int i = 1; i < limit; i++)
	{
		// Compute the next level of the gaussian pyramid
		image = downsample(image, filter, subwindow);

		// Store the current level of the gaussian pyramid
		level.push_back(image);
	}
}


/**
* Destructor for the gaussian pyramid
*/
GaussianPyramid::~GaussianPyramid()
{
	for (int i = 0; i < levels; i++)
	{
		level[i].release();
	}
}


/**
* Return a certain level
* @param i: the index
* @return The level at given index
*/
cv::Mat GaussianPyramid::operator[](int i)
{
	// Check for valid index
	if (i < 0 || i >= levels)
	{
		throw std::out_of_range("Index is out of bounds");
	}
	else
	{
		return level[i];
	}
}


/**
* Return the depth of the pyramid
* @return The depth
*/
int GaussianPyramid::depth()
{
	return levels;
}