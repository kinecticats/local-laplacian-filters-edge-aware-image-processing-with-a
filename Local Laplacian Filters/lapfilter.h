//////////////////////////////////////////////////////////////////////////////////////////////////
//
// Local Laplacian Filters: Edge-aware Image Processing with a Laplacian Pyramid
// For "Praktikum: Bildverarbeitung / Bildkommunikation"
// At the Eberhardt Karls Universität Tübingen, Germany
// 
// This project tries to implement a user friendly GUI to perform image processing
// using the local laplacian filtering method proposed by Paris et al. in their Paper
// "Local Laplacian Filters: Edge-aware Image Processing with a Laplacian Pyramid" (2011).
// The project is using the Gtk+ 3.6.4 GUI Library Bundle for the graphical interface and
// the OpenCV 2.4.9 Open Source Computer Vision Library Bundle for the computation. 
// The code for the HDRImage class is based on Igor Kravtchenko's code (flipcode.com).
//
// Credit goes to Sylvain Paris, Samuel W.Hasinoff, Jan Kautz and Igor Kravtchenko
//
// Author is Adrian Czarkowski
// See me also on Bitbucket, where this project is hosted:
// https://bitbucket.org/kinecticats/local-laplacian-filters-edge-aware-image-processing-with-a
//
//////////////////////////////////////////////////////////////////////////////////////////////////


#ifndef LAPFILTER_H 
#define LAPFILTER_H

#include <math.h>
#include "lapfilter_core.h"


/**
* Wrapper function which calls
* the core function 'lapfilter_core'
* @param image: the input image
* @param color: the remapping function flag
* @param logarithmic: the logaritmic flag
* @param alpha: the edge factor
* @param beta: the scaling factor
* @param sigma_r: the edge sensitivity
* @param noise: the noise level
* return The laplace-filtered image
*/
cv::Mat lapfilter(cv::Mat image, bool color, bool log, float alpha, float beta, float sigma_r, float noise);


#endif