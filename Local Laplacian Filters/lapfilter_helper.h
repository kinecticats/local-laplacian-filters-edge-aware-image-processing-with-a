//////////////////////////////////////////////////////////////////////////////////////////////////
//
// Local Laplacian Filters: Edge-aware Image Processing with a Laplacian Pyramid
// For "Praktikum: Bildverarbeitung / Bildkommunikation"
// At the Eberhardt Karls Universität Tübingen, Germany
// 
// This project tries to implement a user friendly GUI to perform image processing
// using the local laplacian filtering method proposed by Paris et al. in their Paper
// "Local Laplacian Filters: Edge-aware Image Processing with a Laplacian Pyramid" (2011).
// The project is using the Gtk+ 3.6.4 GUI Library Bundle for the graphical interface and
// the OpenCV 2.4.9 Open Source Computer Vision Library Bundle for the computation. 
// The code for the HDRImage class is based on Igor Kravtchenko's code (flipcode.com).
//
// Credit goes to Sylvain Paris, Samuel W.Hasinoff, Jan Kautz and Igor Kravtchenko
//
// Author is Adrian Czarkowski
// See me also on Bitbucket, where this project is hosted:
// https://bitbucket.org/kinecticats/local-laplacian-filters-edge-aware-image-processing-with-a
//
//////////////////////////////////////////////////////////////////////////////////////////////////


#ifndef LAPFILTER_HELPER_H 
#define LAPFILTER_HELPER_H

#include <iostream>
#include <opencv2\imgproc\imgproc.hpp>


/**
* Prints a 3D-matix as RGB
* @param image: the input matrix
* @param name: the name of the matrix
*/
void print3DMat(cv::Mat image, char* name);


/**
* Makes a 3D-matix out of a flat matrix
* by copying three times in the 3rd dimension
* @param matrix: the input matrix
* @return The 3D-matrix
*/
cv::Mat make3DMat(cv::Mat matrix);


/**
* Number of as many pyramid levels
* as possible, up to 1x1 for given image
* @param image: the input image
* return The possible pyramid depth
*/
int numLevels(cv::Mat image);


/**
* Returns a low pass filter used for
* constructing the Gaussian and the
* Laplacian pyramids
* return The low pass filter
*/
cv::Mat get_filter();


/**
* For a parent subwindow [y1 y2 x1 x2], 
* find the corresponding child subwindow 
* at the coarser pyramid level N levels up
* @param parent: the subwindow
* @param N: the depth to iterate
* return Vector of the child subwindow indices
*/
cv::Vec4i child_window(cv::Vec4i parent, int N);


/**
* Calculate the percentiles along the
* columns of a given input n x m matrix
* @param matrix: the input matrix
* @param prc: the percentile to compute
* return Vector of percentiles for given matrix
*/
cv::Mat percentile(cv::Mat matrix, float prc);


/**
* Implements a convolution of a given image
* with a given kernel with zero padding
* @param image: the input image
* @param kernel: the kernel to apply
* return The convoluted image
*/
cv::Mat imfilter(cv::Mat image, cv::Mat kernel);


/**
* Downsamples a given image with a given
* filter while creating the child window
* to the given subwindow
* @param image: the input image
* @param filter: a low pass filter
* @param subwindow: a subwindow of the image
* return The downsampled input image
*/
cv::Mat downsample(cv::Mat image, cv::Mat filter, cv::Vec4i subwindow);


/**
* Upsamples a given image with a given filter 
* @param image: the input image
* @param filter: a low pass filter
* @param subwindow: a subwindow of the image
* return The upsampled input image
*/
cv::Mat upsample(cv::Mat image, cv::Mat filter, cv::Vec4i subwindow);


#endif

