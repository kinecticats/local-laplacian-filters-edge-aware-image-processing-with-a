//////////////////////////////////////////////////////////////////////////////////////////////////
//
// Local Laplacian Filters: Edge-aware Image Processing with a Laplacian Pyramid
// For "Praktikum: Bildverarbeitung / Bildkommunikation"
// At the Eberhardt Karls Universität Tübingen, Germany
// 
// This project tries to implement a user friendly GUI to perform image processing
// using the local laplacian filtering method proposed by Paris et al. in their Paper
// "Local Laplacian Filters: Edge-aware Image Processing with a Laplacian Pyramid" (2011).
// The project is using the Gtk+ 3.6.4 GUI Library Bundle for the graphical interface and 
// the OpenCV 2.4.9 Open Source Computer Vision Library Bundle for the computation. 
// The code for the HDRImage class is based on Igor Kravtchenko's code (flipcode.com).
//
// Credit goes to Sylvain Paris, Samuel W.Hasinoff, Jan Kautz and Igor Kravtchenko
//
// Author is Adrian Czarkowski
// See me also on Bitbucket, where this project is hosted:
// https://bitbucket.org/kinecticats/local-laplacian-filters-edge-aware-image-processing-with-a
//
//////////////////////////////////////////////////////////////////////////////////////////////////


#include "HDRImage.h"


/**
* Constructor for the HDR image
* @param fileName: the image location 
*/
HDRImage::HDRImage(const char *fileName)
{
	// Create working variables
	int i; char str[200]; FILE *file;
	try
	{
		// Try to open the file
		file = fopen(fileName, "rb");
	}
	catch (...)
	{
		return;
	}
	if (!file)
	{
		// Flag "no data stored"
		data = false; 
		return;
	}
	// Read the opened file
	fread(str, 10, 1, file);
	if (memcmp(str, "#?RADIANCE", 10)) 
	{
		// Close the file if not of type .hdr
		fclose(file);
		// Flag "no data stored"
		data = false; 
		return;
	}
	// Position the file indicator at one
	fseek(file, 1, SEEK_CUR);

	// Create working variables
	char cmd[200]; i = 0; char c = 0, oldc;

	// Read file header data into buffer
	while (true) {
		oldc = c;
		c = fgetc(file);
		if (c == 0xa && oldc == 0xa)
			break;
		cmd[i++] = c;
	}
	// Create working variables
	char reso[200]; i = 0;
	
	// Read image header data into buffer
	while (true) {
		c = fgetc(file);
		reso[i++] = c;
		if (c == 0xa)
			break;
	}
	// Initialize 
	int w, h;

	// Search for the line indicating resolutions
	if (!sscanf(reso, "-Y %ld +X %ld", &h, &w)) 
	{
		// Close the broken file 
		fclose(file);
		// Flag "no data stored"
		data = false; 
		return;
	}
	// Store the resolution
	width = w;
	height = h;

	// Allocate memory for the data
	float *cols = new float[w * h * 3];
	this->cols = cols;

	// Allocate a new scanline (RGBE vector for one line)
	RGBE *scanline = new RGBE[w];
	if (!scanline) 
	{
		// Close the file if no scanline could be created
		fclose(file);
		// Flag "no data stored"
		data = false; 
		return;
	}
	// Read image data and convert it to floats 
	for (int y = h - 1; y >= 0; y--) {
		if (decrunch(scanline, w, file) == false)
		{
			break;
		}
		workOnRGBE(scanline, w, cols);
		cols += w * 3;
	}
	// Clean up
	delete[] scanline;
	fclose(file);

	// Flag "data stored"
	data = true; 
	return;
}


/**
* Destructor for the HDR image
*/
HDRImage::~HDRImage()
{
	// Release the data array
	delete[] cols;
}


/**
* Converts a value based on 
* the given exponent to a float
* @param expo: the exponent
* @param val: the value
* @return The converted value as float
*/
float HDRImage::convertComponent(int expo, int val)
{
	float v = val / 256.0f;
	float d = (float)pow((float)2, expo);
	return v * d;
}


/**
* Converts the values of a given RGBE vector
* to RGB and stores them in another vector
* @param scan: the RGBE vector
* @param len: the amount of RGBEs
* @param cols: the RGB vector
*/
void HDRImage::workOnRGBE(RGBE* scan, int len, float* cols)
{
	while (len-- > 0) {
		int expo = scan[0][E] - 128;
		cols[0] = convertComponent(expo, scan[0][R]);
		cols[1] = convertComponent(expo, scan[0][G]);
		cols[2] = convertComponent(expo, scan[0][B]);
		cols += 3;
		scan++;
	}
}


/**
* Reads the values of a given HDR file
* line and stores them in a given vector
* @param scanline: the RGBE vector
* @param len: the current read line length
* @param file: the HDR file
*/
bool HDRImage::decrunch(RGBE* scanline, int len, FILE* file)
{
	// Create the working variables
	int i, j;

	// If the scanline is not within the specified length...
	if (len < MINELEN || len > MAXELEN)
	{
		// ...Use an alternative function
		return oldDecrunch(scanline, len, file);
	}
	// Check if alternative function is needed
	i = fgetc(file);
	if (i != 2) {
		fseek(file, -1, SEEK_CUR);
		return oldDecrunch(scanline, len, file);
	}
	// Extract the next values to...
	scanline[0][G] = fgetc(file);
	scanline[0][B] = fgetc(file);
	i = fgetc(file);
	
	// ...Check if alternative function is needed
	if (scanline[0][G] != 2 || scanline[0][B] & 128) 
	{
		scanline[0][R] = 2;
		scanline[0][E] = i;
		return oldDecrunch(scanline + 1, len - 1, file);
	}
	// Read each component
	for (i = 0; i < 4; i++) 
	{
		for (j = 0; j < len;) 
		{
			unsigned char code = fgetc(file);
			// Check if run-length encoded
			if (code > 128) 
			{
				code &= 127;
				unsigned char val = fgetc(file);
				while (code--)
					scanline[j++][i] = val;
			}
			// Not run-length encoded
			else  
			{
				while (code--)
				{
					scanline[j++][i] = fgetc(file);
				}
			}
		}
	}
	// Return whether end of file reached
	return feof(file) ? false : true;
}


/**
* Reads the values of a given HDR file
* line and stores them in a given vector
* @param scanline: the RGBE vector
* @param len: the current read line length
* @param file: the HDR file
*/
bool HDRImage::oldDecrunch(RGBE *scanline, int len, FILE *file)
{
	// Creates the working variables
	int i; int rshift = 0;

	// Read each component
	while (len > 0) 
	{
		// Read a whole RGBE
		scanline[0][R] = fgetc(file);
		scanline[0][G] = fgetc(file);
		scanline[0][B] = fgetc(file);
		scanline[0][E] = fgetc(file);

		// Check if reached end of file
		if (feof(file))
		{
			return false;
		}
		// Check if values need to be skipped
		if (scanline[0][R] == 1 &&
			scanline[0][G] == 1 &&
			scanline[0][B] == 1) 
		{
			for (i = scanline[0][E] << rshift; i > 0; i--) 
			{
				memcpy(&scanline[0][0], &scanline[-1][0], 4);
				scanline++;
				len--;
			}
			rshift += 8;
		}
		else 
		{
			scanline++;
			len--;
			rshift = 0;
		}
	}
	// Return that the line was read succesfully
	return true;
}


/**
* Return a certain pixel
* @param i: the index
* @return The pixel at given index
*/
float HDRImage::operator[](int i)
{
	// Check for valid index
	if (i < 0 || i > (height * width * 3))
	{
		throw std::out_of_range("Index is out of bounds");
	}
	else
	{
		return cols[i];
	}
}


/**
* Return the height of the image
* @return The height
*/
int HDRImage::getHeight()
{
	return height;
}


/**
* Return the width of the image
* @return The height
*/
int HDRImage::getWidth()
{
	return width;
}