//////////////////////////////////////////////////////////////////////////////////////////////////
//
// Local Laplacian Filters: Edge-aware Image Processing with a Laplacian Pyramid
// For "Praktikum: Bildverarbeitung / Bildkommunikation"
// At the Eberhardt Karls Universität Tübingen, Germany
// 
// This project tries to implement a user friendly GUI to perform image processing
// using the local laplacian filtering method proposed by Paris et al. in their Paper
// "Local Laplacian Filters: Edge-aware Image Processing with a Laplacian Pyramid" (2011).
// The project is using the Gtk+ 3.6.4 GUI Library Bundle for the graphical interface and
// the OpenCV 2.4.9 Open Source Computer Vision Library Bundle for the computation. 
// The code for the HDRImage class is based on Igor Kravtchenko's code (flipcode.com).
//
// Credit goes to Sylvain Paris, Samuel W.Hasinoff, Jan Kautz and Igor Kravtchenko
//
// Author is Adrian Czarkowski
// See me also on Bitbucket, where this project is hosted:
// https://bitbucket.org/kinecticats/local-laplacian-filters-edge-aware-image-processing-with-a
//
//////////////////////////////////////////////////////////////////////////////////////////////////


#include "GUI.h"


/**
* Constructor for the GUI
* @param image: the input image
* @param limit: the maximum depth
*/
GUI::GUI()
{
	// Create a window
	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);

	// Initialize some variables
	const gchar* icon = "icon.ico";
	GError* error = NULL; percent = 0.00; 
	input = false; output = false; process = true;

	// Get the screen properties
	GdkScreen* screen = gtk_window_get_screen(GTK_WINDOW(window));
	width = gdk_screen_get_width(screen);
	height = gdk_screen_get_height(screen);

	// Set the window parameter
	gtk_window_set_title(GTK_WINDOW(window), "Local Laplacian Filters: Image Processing");
	gtk_container_set_border_width(GTK_CONTAINER(window), 10);
	gtk_window_set_default_size(GTK_WINDOW(window), width / 2, height / 2);
	g_signal_connect(G_OBJECT(window), "destroy", G_CALLBACK(gtk_main_quit), NULL);
	gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
	gtk_window_set_icon_from_file(GTK_WINDOW(window), icon, &error);
	gtk_window_set_resizable(GTK_WINDOW(window), FALSE);

	// Initialize the boxes to hold all widgets
	main_box		= gtk_box_new(GTK_ORIENTATION_VERTICAL, 10);
	top_box			= gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 10);
	left_box		= gtk_box_new(GTK_ORIENTATION_VERTICAL, 10);
	right_box		= gtk_box_new(GTK_ORIENTATION_VERTICAL, 10);
	vbox_slider		= gtk_box_new(GTK_ORIENTATION_VERTICAL, 10);
	alpha			= gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 10);
	alpha_help		= gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	beta			= gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 10);
	beta_help		= gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	sigma			= gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 10);
	sigma_help		= gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	noise			= gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 10);
	noise_help		= gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	hbox_radios		= gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 30);
	filler_radios	= gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
	vbox_radios		= gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	vbox_radios_top = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	vbox_radios_bot	= gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	left_buttons	= gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 10);
	right_buttons	= gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 10);
	progressbar		= gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 10);
	left_image		= gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 10);
	right_image		= gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 10);
	input_frame		= gtk_frame_new("Input Image");
	output_frame	= gtk_frame_new("Output Image");

	// Initialize the radio button listener
	radio_top = gtk_radio_button_new(NULL);
	radio_bot = gtk_radio_button_new(NULL);

	// Create seperators
	left_separator  = gtk_separator_new(GTK_ORIENTATION_HORIZONTAL);
	right_separator = gtk_separator_new(GTK_ORIENTATION_HORIZONTAL);
	radio_separator = gtk_separator_new(GTK_ORIENTATION_HORIZONTAL);
	image_separator = gtk_separator_new(GTK_ORIENTATION_VERTICAL);

	// Put the boxes into the main window
	gtk_container_add(GTK_CONTAINER(window), main_box);
	gtk_container_add(GTK_CONTAINER(main_box), top_box);
	gtk_container_add(GTK_CONTAINER(top_box), left_box);
	gtk_container_add(GTK_CONTAINER(top_box), image_separator);
	gtk_container_add(GTK_CONTAINER(top_box), right_box);
	gtk_container_add(GTK_CONTAINER(left_box), input_frame);
	gtk_container_add(GTK_CONTAINER(input_frame), left_image);
	gtk_container_add(GTK_CONTAINER(left_box), left_buttons);
	gtk_container_add(GTK_CONTAINER(left_box), left_separator);
	gtk_container_add(GTK_CONTAINER(left_box), vbox_slider);
	gtk_container_add(GTK_CONTAINER(vbox_slider), alpha);
	gtk_container_add(GTK_CONTAINER(alpha), alpha_help);
	gtk_container_add(GTK_CONTAINER(vbox_slider), beta);
	gtk_container_add(GTK_CONTAINER(beta), beta_help);
	gtk_container_add(GTK_CONTAINER(vbox_slider), sigma);
	gtk_container_add(GTK_CONTAINER(sigma), sigma_help);
	gtk_container_add(GTK_CONTAINER(vbox_slider), noise);
	gtk_container_add(GTK_CONTAINER(noise), noise_help);
	gtk_container_add(GTK_CONTAINER(right_box), output_frame);
	gtk_container_add(GTK_CONTAINER(output_frame), right_image);
	gtk_container_add(GTK_CONTAINER(right_box), right_buttons);
	gtk_container_add(GTK_CONTAINER(right_box), right_separator);
	gtk_container_add(GTK_CONTAINER(right_box), hbox_radios);
	gtk_container_add(GTK_CONTAINER(hbox_radios), filler_radios);
	gtk_container_add(GTK_CONTAINER(hbox_radios), vbox_radios);
	gtk_container_add(GTK_CONTAINER(vbox_radios), vbox_radios_top);
	gtk_container_add(GTK_CONTAINER(vbox_radios), radio_separator);
	gtk_container_add(GTK_CONTAINER(vbox_radios), vbox_radios_bot);
	gtk_container_add(GTK_CONTAINER(main_box), progressbar);

	// Set the image boxes to an initial size
	gtk_widget_set_size_request(left_image, width / 3, height / 3);
	gtk_widget_set_size_request(right_image, width / 3, height / 3);

	// Create the button to load a LDR image 
	button_LDR = gtk_button_new_with_label("Load an Image");
	ldr_id = g_signal_connect(button_LDR, "clicked", G_CALLBACK(loadLDR), (GUI*)this);
	gtk_box_pack_start(GTK_BOX(left_buttons), button_LDR, TRUE, TRUE, 10);

	// Create the button to load a HDR image
	button_HDR = gtk_button_new_with_label("Load HDR Image");
	hdr_id = g_signal_connect(button_HDR, "clicked", G_CALLBACK(loadHDR), (GUI*)this);
	gtk_box_pack_start(GTK_BOX(left_buttons), button_HDR, TRUE, TRUE, 10);

	// Create the button to start the processing
	button_Start = gtk_button_new_with_label("Start Processing");
	process_id = g_signal_connect(button_Start, "clicked", G_CALLBACK(startProcessing), (GUI*)this);
	cancel_id = g_signal_connect(button_Start, "clicked", G_CALLBACK(stopProcessing), (GUI*)this);
	gtk_box_pack_start(GTK_BOX(right_buttons), button_Start, TRUE, TRUE, 10);
	g_signal_handler_block(button_Start, cancel_id);

	// Create the button to save the result as PNG
	button_Save = gtk_button_new_with_label("Save the result");
	save_id = g_signal_connect(button_Save, "clicked", G_CALLBACK(saveResult), (GUI*)this);
	gtk_box_pack_start(GTK_BOX(right_buttons), button_Save, TRUE, TRUE, 10);

	// Create the slider for alpha
	alpha_button = gtk_button_new_with_label("?");
	alpha_id = g_signal_connect(alpha_button, "clicked", G_CALLBACK(alphaHelp), (GUI*)this);
	alpha_slider = gtk_scale_new_with_range(GTK_ORIENTATION_HORIZONTAL, 0.00, 5.00, 0.01);
	gtk_range_set_value((GtkRange*)alpha_slider, 1.00);
	gtk_box_pack_start(GTK_BOX(alpha_help), alpha_button, TRUE, TRUE, 10);
	gtk_box_pack_start(GTK_BOX(alpha), alpha_slider, TRUE, TRUE, 10);

	// Create the slider for beta
	beta_button = gtk_button_new_with_label("?");
	beta_id = g_signal_connect(beta_button, "clicked", G_CALLBACK(betaHelp), (GUI*)this);
	beta_slider = gtk_scale_new_with_range(GTK_ORIENTATION_HORIZONTAL, 0.00, 5.00, 0.01);
	gtk_range_set_value((GtkRange*)beta_slider, 1.00);
	gtk_box_pack_start(GTK_BOX(beta_help), beta_button, TRUE, TRUE, 10);
	gtk_box_pack_start(GTK_BOX(beta), beta_slider, TRUE, TRUE, 10);

	// Create the slider for sigma_r
	sigma_button = gtk_button_new_with_label("?");
	sigma_id = g_signal_connect(sigma_button, "clicked", G_CALLBACK(sigmaHelp), (GUI*)this);
	sigma_slider = gtk_scale_new_with_range(GTK_ORIENTATION_HORIZONTAL, 0.00, 5.00, 0.01);
	gtk_range_set_value((GtkRange*)sigma_slider, 0.50);
	gtk_box_pack_start(GTK_BOX(sigma_help), sigma_button, TRUE, TRUE, 10);
	gtk_box_pack_start(GTK_BOX(sigma), sigma_slider, TRUE, TRUE, 10);

	// Create the slider for the noise
	noise_button = gtk_button_new_with_label("?");
	noise_id = g_signal_connect(noise_button, "clicked", G_CALLBACK(noiseHelp), (GUI*)this);
	noise_slider = gtk_scale_new_with_range(GTK_ORIENTATION_HORIZONTAL, 0.00, 1.00, 0.01);
	gtk_range_set_value((GtkRange*)noise_slider, 0.50);
	gtk_box_pack_start(GTK_BOX(noise_help), noise_button, TRUE, TRUE, 10);
	gtk_box_pack_start(GTK_BOX(noise), noise_slider, TRUE, TRUE, 10);

	// Create the radio button for linear processing
	linear_radio = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(radio_top), "Linear:\nProcess the images values as given (recommended for PNG)");
	gtk_box_pack_start(GTK_BOX(vbox_radios_top), linear_radio, TRUE, TRUE, 0);

	// Create the radio button for logarithmic processing
	logarithmic_radio = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(radio_top), "Logarithmic:\nProcess the image values in logarithmic scale (recommended for HDR)");
	gtk_box_pack_start(GTK_BOX(vbox_radios_top), logarithmic_radio, TRUE, TRUE, 10);

	// Create the radio button for color processing
	color_radio = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(radio_bot), "Color:\nOperate on the color values and the light intensity");
	gtk_box_pack_start(GTK_BOX(vbox_radios_bot), color_radio, TRUE, TRUE, 10);

	// Create the radio button for luminance processing
	luminance_radio = gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(radio_bot), "Luminance:\nOperate on the light intensity only and leave color unchanged");
	gtk_box_pack_start(GTK_BOX(vbox_radios_bot), luminance_radio, TRUE, TRUE, 10);

	// Set the active radio buttons
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(linear_radio), TRUE);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(color_radio), TRUE);

	// Initialize a progressbar
	progress = gtk_progress_bar_new();
	gtk_box_pack_start(GTK_BOX(progressbar), progress, TRUE, TRUE, 10);

	// Initialize the images
	input_image = gtk_image_new();
	output_image = gtk_image_new();
	gtk_box_pack_start(GTK_BOX(left_image), input_image, TRUE, TRUE, 10);
	gtk_box_pack_start(GTK_BOX(right_image), output_image, TRUE, TRUE, 10);

	//updateProgressBar(button_Start, this);
	g_timeout_add(10, (GSourceFunc)updateProgressBar, (GUI*)this);
	
	// Show all widgets
	gtk_widget_show_all(window);
}


/**
* Destructor for the GUI
*/
GUI::~GUI()
{
	// Clean up
	gtk_widget_destroy(main_box);
	gtk_widget_destroy(top_box);
	gtk_widget_destroy(left_box);
	gtk_widget_destroy(right_box);
	gtk_widget_destroy(left_image);
	gtk_widget_destroy(right_image);
	gtk_widget_destroy(input_image);
	gtk_widget_destroy(output_image);
	gtk_widget_destroy(left_buttons);
	gtk_widget_destroy(button_LDR);
	gtk_widget_destroy(button_HDR);
	gtk_widget_destroy(right_buttons);
	gtk_widget_destroy(button_Start);
	gtk_widget_destroy(button_Save);
	gtk_widget_destroy(vbox_slider);
	gtk_widget_destroy(alpha);
	gtk_widget_destroy(alpha_help);
	gtk_widget_destroy(alpha_button);
	gtk_widget_destroy(beta_slider);
	gtk_widget_destroy(beta);
	gtk_widget_destroy(beta_help);
	gtk_widget_destroy(beta_slider);
	gtk_widget_destroy(beta_button);
	gtk_widget_destroy(sigma);
	gtk_widget_destroy(sigma_help);
	gtk_widget_destroy(sigma_slider);
	gtk_widget_destroy(sigma_button);
	gtk_widget_destroy(noise);
	gtk_widget_destroy(noise_help);
	gtk_widget_destroy(noise_slider);
	gtk_widget_destroy(noise_button);
	gtk_widget_destroy(hbox_radios);
	gtk_widget_destroy(filler_radios);
	gtk_widget_destroy(vbox_radios);
	gtk_widget_destroy(vbox_radios_top);
	gtk_widget_destroy(linear_radio);
	gtk_widget_destroy(logarithmic_radio);
	gtk_widget_destroy(vbox_radios_bot);
	gtk_widget_destroy(color_radio);
	gtk_widget_destroy(luminance_radio);
	gtk_widget_destroy(progressbar);
	gtk_widget_destroy(progress);
	gtk_widget_destroy(radio_top);
	gtk_widget_destroy(radio_bot);
	gtk_widget_destroy(left_separator);
	gtk_widget_destroy(right_separator);
	gtk_widget_destroy(radio_separator);
}


/**
* Load a PNG image and display it
* @param widget: the calling widget
* @param gui: this class 
*/
void GUI::loadLDR(GtkWidget* widget, GUI* gui)
{
	// Initialize working variables
	gui->percent = 0.00;
	char* filename = "";
	GtkWidget* dialog;
	cv::Mat image;

	// Reset the output image
	gtk_image_clear((GtkImage*)gui->output_image);
	gui->output = false;

	// Create file browsing dialog
	dialog = gtk_file_chooser_dialog_new("Open File", 
		GTK_WINDOW(widget), 
		GTK_FILE_CHOOSER_ACTION_OPEN, 
		GTK_STOCK_CANCEL, 
		GTK_RESPONSE_CANCEL, 
		GTK_STOCK_OPEN, 
		GTK_RESPONSE_ACCEPT, 
		NULL);

	if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_ACCEPT)
	{
		// Get the filename of the chosen file
		filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));

		// Stop the file browser
		gtk_widget_destroy(dialog);
	}
	else
	{
		// Stop the file browser
		gtk_widget_destroy(dialog); return;
	}
	try
	{
		// Read the chosen file
		gui->cv_input_image = cv::imread(filename, cv::IMREAD_COLOR);
	}
	catch (...)
	{
		// Handle the error
		loadError(gui); return;
	}
	// Normalize the image
	cv::normalize(gui->cv_input_image, image, 0, 255, cv::NORM_MINMAX, CV_8UC3);

	// Check if the image fits the screen; If not, resize the displayed image (not the processed one)
	bool wideEnough = (gui->width / 3) > image.cols;
	bool highEnough = (gui->height / 3) > image.rows;
	if (!wideEnough || !highEnough)
	{
		// Show information
		gui->showInfo(gui, (gchar*)"Due to its size, the image was resized to be displayed on the screen\n\nNevertheless the file will be processed in its original dimensions");
		if (!wideEnough && !highEnough)
		{
			// Resize the displayed image in width and height
			cv::Size size((gui->width / 2) - 100, (gui->height / 2) - 100);
			cv::resize(image, image, size);
		}
		else
		{
			if (!wideEnough)
			{
				// Resize the displayed image in width
				cv::Size size((gui->width / 2) - 100, image.rows);
				cv::resize(image, image, size);
			}
			else
			{
				// Resize the displayed image in height
				cv::Size size(image.cols, (gui->height / 2) - 100);
				cv::resize(image, image, size);
			}
		}
	}
	// Convert it from [0,255] to [0,1]
	gui->cv_input_image.convertTo(gui->cv_input_image, CV_32FC3);
	gui->cv_input_image = gui->cv_input_image / 255;
	try
	{
		// Since the pixbuffer throws errors, 
		// the image will be temporaly written as file
		cv::imwrite("tmp_Input_for_LoLaFi_EaIpLp.png", image);
	}
	catch (...)
	{
		// Handle the error
		loadError(gui); return;
	}
	try
	{
		// Replace the old image by the new one and clear the output image
		gtk_image_set_from_file((GtkImage*)gui->input_image, "tmp_Input_for_LoLaFi_EaIpLp.png");
	}
	catch (...)
	{
		// Handle the error
		loadError(gui); return;
	}
	// Remove the temporaly file
	if (remove("tmp_Input_for_LoLaFi_EaIpLp.png") != 0)
	{
		perror("Error deleting file");
	}
	else
	{
		puts("File successfully deleted");
	}
	// Resize the window to fit the images
	gtk_window_resize(GTK_WINDOW(gui->window), 1, 1);
	gtk_widget_set_size_request(gui->left_image, image.cols, image.rows);
	gtk_widget_set_size_request(gui->right_image, image.cols, image.rows);

	// Flag input image as existing
	gui->input = true;

	// Stop the file browser
	gtk_widget_destroy(dialog);

	// Show all widgets
	gtk_widget_show_all(gui->window);
}


/**
* Load a HDR image and display it
* @param widget: the calling widget
* @param gui: this class
*/
void GUI::loadHDR(GtkWidget* widget, GUI* gui)
{
	// Initialize working variables
	gui->percent = 0.00;
	char* filename = "";
	GtkWidget* dialog;
	cv::Vec3f pixel;
	cv::Mat image; 
	int i = 0;

	// Reset the output image
	gtk_image_clear((GtkImage*)gui->output_image);
	gui->output = false;

	// Create file browsing dialog
	dialog = gtk_file_chooser_dialog_new("Open File",
		GTK_WINDOW(widget),
		GTK_FILE_CHOOSER_ACTION_OPEN,
		GTK_STOCK_CANCEL,
		GTK_RESPONSE_CANCEL,
		GTK_STOCK_OPEN,
		GTK_RESPONSE_ACCEPT,
		NULL);

	if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_ACCEPT)
	{
		// Get the filename of the chosen file
		filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));

		// Stop the file browser
		gtk_widget_destroy(dialog);
	}
	else
	{
		// Stop the file browser
		gtk_widget_destroy(dialog); return;
	}
	// Read the chosen file
	HDRImage hdr_image = HDRImage(filename);

	// Create a matrix to hold the HDR image data
	gui->cv_input_image = cv::Mat(hdr_image.getHeight(), hdr_image.getWidth(), CV_32FC3);

	// Fill the matrix with the HDR image data
	for (int y = 0; y < hdr_image.getHeight(); y++)
	{
		for (int x = 0; x < hdr_image.getWidth(); x++)
		{
			pixel[2] = hdr_image[i]; i++;
			pixel[1] = hdr_image[i]; i++;
			pixel[0] = hdr_image[i]; i++;
			gui->cv_input_image.at<cv::Vec3f>(y, x) = pixel;
		}
	}
	// Normalize the image
	image = gui->cv_input_image * 255;

	// Check if the image fits the screen; If not, resize the displayed image (not the processed one)
	bool wideEnough = (gui->width / 3) > image.cols;
	bool highEnough = (gui->height / 3) > image.rows;
	if (!wideEnough || !highEnough)
	{
		// Show information
		gui->showInfo(gui, (gchar*)"Due to its size, the image was resized to be displayed on the screen\n\nNevertheless the file will be processed in its original dimensions");
		if (!wideEnough && !highEnough)
		{
			// Resize the displayed image in width and height
			cv::Size size((gui->width / 2) - 100, (gui->height / 2) - 100);
			cv::resize(image, image, size);
		}
		else
		{
			if (!wideEnough)
			{
				// Resize the displayed image in width
				cv::Size size((gui->width / 2) - 100, image.rows);
				cv::resize(image, image, size);
			}
			else
			{
				// Resize the displayed image in height
				cv::Size size(image.cols, (gui->height / 2) - 100);
				cv::resize(image, image, size);
			}
		}
	}
	try
	{
		// Since the pixbuffer throws errors, 
		// the image will be temporaly written as file
		cv::imwrite("tmp_Input_for_LoLaFi_EaIpLp.png", image);
	}
	catch (...)
	{
		// Handle the error
		loadError(gui); return;
	}
	try
	{
		// Replace the old image by the new one and clear the output image
		gtk_image_set_from_file((GtkImage*)gui->input_image, "tmp_Input_for_LoLaFi_EaIpLp.png");
	}
	catch (...)
	{
		// Handle the error
		loadError(gui); return;
	}
	// Remove the temporaly file
	if (remove("tmp_Input_for_LoLaFi_EaIpLp.png") != 0)
	{
		perror("Error deleting file");
	}
	else
	{
		puts("File successfully deleted");
	}
	// Resize the window to fit the images
	gtk_window_resize(GTK_WINDOW(gui->window), 1, 1);
	gtk_widget_set_size_request(gui->left_image, image.cols, image.rows);
	gtk_widget_set_size_request(gui->right_image, image.cols, image.rows);

	// Flag input image as existing
	gui->input = true;

	// Show all widgets
	gtk_widget_show_all(gui->window);
}


/**
* Process the input image
* @param widget: the calling widget
* @param gui: this class
*/
gboolean GUI::startProcessing(GtkWidget* widget, GUI* gui)
{
	// Check for image to process
	if (!gui->input)
	{
		// Callback finished
		return FALSE;
	}
	// Reset the output image
	gtk_image_clear((GtkImage*)gui->output_image);
	gui->output = false;

	// Block all other buttons
	g_signal_handler_block(gui->button_LDR, gui->ldr_id);
	g_signal_handler_block(gui->button_HDR, gui->hdr_id);
	g_signal_handler_block(gui->button_Start, gui->process_id);
	g_signal_handler_block(gui->button_Save, gui->save_id);
	g_signal_handler_block(gui->alpha_button, gui->alpha_id);
	g_signal_handler_block(gui->beta_button, gui->beta_id);
	g_signal_handler_block(gui->sigma_button, gui->sigma_id);
	g_signal_handler_block(gui->noise_button, gui->noise_id);

	// Change label of the processing button
	gtk_button_set_label(GTK_BUTTON(gui->button_Start), "Cancel Processing");

	// Unblock the cancel function
	g_signal_handler_unblock(gui->button_Start, gui->cancel_id);

	// Initialize the processing state
	gui->process = true;

	// Initialize the radio button result
	bool logarithmic = true;
	bool color = true;

	// Fetch the parameter from the GUI
	float alpha = gtk_range_get_value((GtkRange*)gui->alpha_slider);
	float beta = gtk_range_get_value((GtkRange*)gui->beta_slider);
	float sigma = gtk_range_get_value((GtkRange*)gui->sigma_slider);
	float noise = gtk_range_get_value((GtkRange*)gui->noise_slider);
	if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(gui->logarithmic_radio)) == FALSE) logarithmic = false;
	if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(gui->luminance_radio)) == TRUE) color = false;

	// Initilialize the working variables for the processing
	cv::Mat image_lum, result_lum, lum_ratio, result, image;
	
	// Copy the input image to a variable
	gui->cv_input_image.copyTo(image);

	// Adjust parameters if not linear 
	if (logarithmic)
	{
		sigma = log(sigma);
	}
	// Adjust parameters if not in color
	if (!color)
	{
		image_lum = luminance(image);
		cv::divide(image, make3DMat(image_lum + DBL_EPSILON), lum_ratio);
		image_lum.copyTo(image);
	}
	// Check if processing is needed
	if (alpha == 1 && beta == 1)
	{
		image.copyTo(result);
	}
	else
	{
		// Preprocess the input if logarithmic
		if (logarithmic)
		{
			// Convert the image to the logarithmic domain
			cv::log(image + DBL_EPSILON, image);
		}
		// Store the size of the first pyramid level
		int height = image.rows;
		int width = image.cols;

		// Create the initial subwindow
		cv::Vec4i subwindow = { 1, height, 1, width };

		// Create the gaussian pyramid
		GaussianPyramid GP(image, numLevels(image));

		// Allocate memory for the laplacian pyramid
		LaplacianPyramid LP(cv::Mat::zeros(image.size(), image.type()), numLevels(image), subwindow);

		// Initialize the working variables
		int hw, xf, yf, xfc, yfc; float gray, xfclevel, yfclevel;
		cv::Mat_<cv::Vec3f> rgb(1, 1, cv::Vec3f(0, 0, 0)); 
		cv::Mat Iremap, Isub; cv::Range xrng, yrng; 
		float level_factor, row_factor;

		// Perform the efficent laplacian filtering with O(N log N)
		for (int level = 0; level < LP.depth() - 1; level++)
		{
			for (int y = 0; y < GP[level].rows; y++)
			{
				// Output the progress
				level_factor = level; level_factor = level_factor / (LP.depth() - 1);
				row_factor = y; row_factor = row_factor / GP[level].rows; row_factor = row_factor / (LP.depth() - 1);
				gui->percent = level_factor + row_factor;

				for (int x = 0; x < GP[level].cols; x++)
				{
					// Calculate half-width of full-res footprint
					hw = 3 * pow(2, level + 1) - 2;

					// Coords in full-res image corresponding to [level, y, x]
					xf = x * pow(2, level);
					yf = y * pow(2, level);

					// Subwindow in full-res image needed to evaluate [level, y, x) in result
					xrng = cv::Range(std::max(0, xf - hw), std::min(image.cols, xf + hw + 1));
					yrng = cv::Range(std::max(0, yf - hw), std::min(image.rows, yf + hw + 1));

					// Extracted subwindow from input image
					Isub = image(yrng, xrng);

					// Check which processing style
					if (color)
					{
						// Calculate the remapped subwindow
						rgb = GP[level].at<cv::Vec3f>(y, x); // g0 = [BGR]
						Iremap = r_color(Isub, rgb, alpha, beta, sigma, noise);
					}
					else
					{
						// Calculate the remapped subwindow
						gray = GP[level].at<float>(y, x); // g0 = [gray]
						Iremap = r_gray(Isub, gray, alpha, beta, sigma, noise);
					}
					// Distribute the subwindow indices over the subwindow vector
					subwindow[0] = (yrng.start + 1);
					subwindow[1] = yrng.end;
					subwindow[2] = (xrng.start + 1);
					subwindow[3] = xrng.end;

					// Compute Laplacian pyramid for the remapped subwindow
					LaplacianPyramid Lremap(Iremap, level + 2, subwindow);

					// Compute index of [level, y, x] within subwindow, at full-res and at current pyramid level
					xfc = xf - xrng.start; xfclevel = xfc / pow(2, level); xfclevel = floor(xfclevel);
					yfc = yf - yrng.start; yfclevel = yfc / pow(2, level); yfclevel = floor(yfclevel);

					// Check the number of channels
					if (image.channels() == 3)
					{
						// Set coefficient in result based on the corresponding coefficient in the remapped pyramid
						LP[level].at<cv::Vec3f>(y, x) = Lremap[level].at<cv::Vec3f>(yfclevel, xfclevel);
					}
					if (image.channels() == 1)
					{
						// Set coefficient in result based on the corresponding coefficient in the remapped pyramid
						LP[level].at<float>(y, x) = Lremap[level].at<float>(yfclevel, xfclevel);
					}
					// Update the GUI
					while (gtk_events_pending())
					{
						gtk_main_iteration();
					}
					// Check if processing should be stopped
					if (!(gui->process))
					{
						// Reestablish former state
						processReset(gui);

						// Callback finished
						return FALSE;
					}
				}
			}
		}
		// Assign the lowest gauss pyramid level to the lowest laplace pyramid level
		GP[GP.depth() - 1].copyTo(LP[LP.depth() - 1]);

		// Reconstruct the image
		result = reconstruct(LP);

		// Postprocess the result if logarithmic
		if (logarithmic)
		{
			cv::exp(result, result);
			result = result - DBL_EPSILON;
		}
	}
	// General postprocessing
	if (logarithmic && beta <= 1)
	{
		// For tone mapping, remap middle 99% of intensities
		// to fixed dynamic range using a gamma curve
		int dyn_rng = 100;
		float prc_clip = 0.5;
		result_lum = luminance(result);
		result_lum = (result_lum.reshape(1, 1)).t();
		float result_max_clip = percentile(result_lum, 100 - prc_clip).at<float>(0, 0);
		float result_min_clip = percentile(result_lum, prc_clip).at<float>(0, 0);
		float dyn_rng_clip = result_max_clip / result_min_clip;
		float exponent = log(dyn_rng) / log(dyn_rng_clip);
		result = cv::max(0, result / result_max_clip);
		cv::pow(result, exponent, result);
	}
	if (!color)
	{
		// Reintroduce color ratios
		cv::multiply(make3DMat(result), lum_ratio, result);
	}
	// Clip out of bounds intensities
	result = cv::max(0, result);
	if (beta <= 1)
	{
		result = cv::min(1, result);
	}
	// For tone mapping, gamma correct linear intensities for display
	if (logarithmic && beta <= 1)
	{
		float gamma = 2.2;
		cv::pow(result, 1 / gamma, result);
	}
	// Normalize the image
	cv::normalize(result, result, 0, 255, cv::NORM_MINMAX, CV_8UC3);

	// Store the result
	result.copyTo(gui->cv_output_image);

	// Flag output as existing
	gui->output = true;

	// Check if the image fits the screen; If not, resize the displayed image (not the processed one)
	bool wideEnough = (gui->width / 3) > result.cols;
	bool highEnough = (gui->height / 3) > result.rows;
	if (!wideEnough || !highEnough)
	{
		// Show information
		gui->showInfo(gui, (gchar*)"Due to its size, the image was resized to be displayed on the screen\n\nNevertheless the file was processed in its original dimensions");
		if (!wideEnough && !highEnough)
		{
			// Resize the displayed image in width and height
			cv::Size size((gui->width / 2) - 100, (gui->height / 2) - 100);
			cv::resize(result, result, size);
		}
		else
		{
			if (!wideEnough)
			{
				// Resize the displayed image in width
				cv::Size size((gui->width / 2) - 100, result.rows);
				cv::resize(result, result, size);
			}
			else
			{
				// Resize the displayed image in height
				cv::Size size(gui->cv_output_image.cols, (gui->height / 2) - 100);
				cv::resize(result, result, size);
			}
		}
	}
	try
	{
		// Since the pixbuffer throws errors, 
		// the image will be temporaly written as file
		cv::imwrite("tmp_Output_for_LoLaFi_EaIpLp.png", result);
	}
	catch (...)
	{
		// Show information
		gui->showInfo(gui, (gchar*)"An error occured while trying to display the processed image! Try to save it");

		// Handle the error
		processReset(gui);

		// Callback finished
		return FALSE;
	}
	try
	{
		// Replace the old image by the new one
		gtk_image_set_from_file((GtkImage*)gui->output_image, "tmp_Output_for_LoLaFi_EaIpLp.png");
	}
	catch (...)
	{
		// Show information
		gui->showInfo(gui, (gchar*)"An error occured while trying to display the processed image! Try to save it");

		// Handle the error
		processReset(gui);

		// Callback finished
		return FALSE;
	}
	// Remove the temporaly file
	if (remove("tmp_Output_for_LoLaFi_EaIpLp.png") != 0)
	{
		perror("Error deleting file");
	}
	else
	{
		puts("File successfully deleted");
	}
	// Show all widgets
	gtk_widget_show_all(gui->window);

	// Reestablish former state
	processReset(gui);

	// Callback finished
	return FALSE;
}


/**
* Save the output image
* @param widget: the calling widget
* @param gui: this class
*/
void GUI::saveResult(GtkWidget* widget, GUI* gui)
{
	// Check for image to save
	if (!gui->output)
	{
		return;
	}
	// Initialize the dialog
	GtkWidget *dialog;

	// Create file browsing dialog
	dialog = gtk_file_chooser_dialog_new("Save File", 
		GTK_WINDOW(widget), 
		GTK_FILE_CHOOSER_ACTION_SAVE, 
		GTK_STOCK_CANCEL, 
		GTK_RESPONSE_CANCEL, 
		GTK_STOCK_SAVE, 
		GTK_RESPONSE_ACCEPT, 
		NULL);
	gtk_file_chooser_set_do_overwrite_confirmation(GTK_FILE_CHOOSER(dialog), TRUE);

	// Set the properties of the browser
	gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(dialog), "");
	gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(dialog), "Untitled Image.png");

	// Open the file browser
	if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_ACCEPT)
	{
		// Get the desired filename
		char *filename;
		filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
		try
		{
			// Write the file
			cv::imwrite(filename, gui->cv_output_image);
		}
		catch (...)
		{
			// Stop the file browser
			gtk_widget_destroy(dialog);

			// Show information
			gui->showInfo(gui, (gchar*)"An error occured while saving the image\n\nIs the chosen file extension valid?"); return;
		}
	}
	// Stop the file browser
	gtk_widget_destroy(dialog);
}


/**
* Update the percent value of the progressbar
* @param gui: this class
*/
gboolean GUI::updateProgressBar(GUI* gui)
{
	// Set a new value for the progressbar
	gtk_progress_bar_set_fraction((GtkProgressBar*)gui->progress, gui->percent);

	// Show the updated progressbar
	gtk_widget_show_all(gui->window);

	// Expecting next call
	return TRUE;
}


/**
* Stop the image processing
* @param widget: the calling widget
* @param gui: this class
*/
void GUI::stopProcessing(GtkWidget* widget, GUI* gui)
{
	// Set a new value for the processing
	gui->process = false;
}


gboolean GUI::showInfo(GUI* gui, const gchar* text)
{
	// Initialize the dialog 
	GtkWidget *dialog;

	// Create the pop up window
	dialog = gtk_message_dialog_new(GTK_WINDOW(gui->window),
		GTK_DIALOG_DESTROY_WITH_PARENT,
		GTK_MESSAGE_INFO,
		GTK_BUTTONS_OK,
		text, "title");

	// Set a title and run it
	gtk_window_set_title(GTK_WINDOW(dialog), "Information");
	gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);

	// Callback finished
	return FALSE;
}


gboolean GUI::processReset(GUI* gui)
{
	// Unblock all other buttons
	g_signal_handler_unblock(gui->button_LDR, gui->ldr_id);
	g_signal_handler_unblock(gui->button_HDR, gui->hdr_id);
	g_signal_handler_unblock(gui->button_Start, gui->process_id);
	g_signal_handler_unblock(gui->button_Save, gui->save_id);
	g_signal_handler_unblock(gui->alpha_button, gui->alpha_id);
	g_signal_handler_unblock(gui->beta_button, gui->beta_id);
	g_signal_handler_unblock(gui->sigma_button, gui->sigma_id);
	g_signal_handler_unblock(gui->noise_button, gui->noise_id);

	// Reestablish the old label
	gtk_button_set_label(GTK_BUTTON(gui->button_Start), "Start Processing");

	// Block the cancel function
	g_signal_handler_block(gui->button_Start, gui->cancel_id);

	// Reset the progress value
	gui->percent = 0.00;

	// Flag the processing as finished
	gui->process = false;

	// Callback finished
	return FALSE;
}


gboolean GUI::loadError(GUI* gui)
{
	// Show information
	gui->showInfo(gui, (gchar*)"An error occured while loading the image file");

	// Set the image boxes to their initial size
	gtk_widget_set_size_request(gui->left_image, gui->width / 3, gui->height / 3);
	gtk_widget_set_size_request(gui->right_image, gui->width / 3, gui->height / 3);

	// Reset the input image
	gtk_image_clear((GtkImage*)gui->input_image);
	gui->input = false;

	// Callback finished
	return FALSE;
}


gboolean GUI::alphaHelp(GtkWidget* widget, GUI* gui)
{
	// Show information
	gui->showInfo(gui, (gchar*)"Alpha: This is the value for the detail amendment\n\n- Set it to less than 1 to enhance details\n\n- Set it to more than 1 to smooth details");

	// Callback finished
	return FALSE;
}


gboolean GUI::betaHelp(GtkWidget* widget, GUI* gui)
{
	// Show information
	gui->showInfo(gui, (gchar*)"Beta: This is the value for the tone amendment\n\n- Set it to less than 1 to do tone mapping\n\n- Set it to more than 1 to do inverse tone mapping");

	// Callback finished
	return FALSE;
}


gboolean GUI::sigmaHelp(GtkWidget* widget, GUI* gui)
{
	// Show information
	gui->showInfo(gui, (gchar*)"Sigma: This is the value for the detail threshold\n\n- The lower the value is set, the smaller the edges which count as details");

	// Callback finished
	return FALSE;
}


gboolean GUI::noiseHelp(GtkWidget* widget, GUI* gui)
{
	// Show information
	gui->showInfo(gui, (gchar*)"Noise: This is the value for the image quality of the input image\n\n- The lower the value is set, the better the perceived quality of the input image");

	// Callback finished
	return FALSE;
}