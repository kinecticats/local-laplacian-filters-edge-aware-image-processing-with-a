//////////////////////////////////////////////////////////////////////////////////////////////////
//
// Local Laplacian Filters: Edge-aware Image Processing with a Laplacian Pyramid
// For "Praktikum: Bildverarbeitung / Bildkommunikation"
// At the Eberhardt Karls Universität Tübingen, Germany
// 
// This project tries to implement a user friendly GUI to perform image processing
// using the local laplacian filtering method proposed by Paris et al. in their Paper
// "Local Laplacian Filters: Edge-aware Image Processing with a Laplacian Pyramid" (2011).
// The project is using the Gtk+ 3.6.4 GUI Library Bundle for the graphical interface and
// the OpenCV 2.4.9 Open Source Computer Vision Library Bundle for the computation. 
// The code for the HDRImage class is based on Igor Kravtchenko's code (flipcode.com).
//
// Credit goes to Sylvain Paris, Samuel W.Hasinoff, Jan Kautz and Igor Kravtchenko
//
// Author is Adrian Czarkowski
// See me also on Bitbucket, where this project is hosted:
// https://bitbucket.org/kinecticats/local-laplacian-filters-edge-aware-image-processing-with-a
//
//////////////////////////////////////////////////////////////////////////////////////////////////


#ifndef HDRIMAGE_H 
#define HDRIMAGE_H 


#define _CRT_SECURE_NO_DEPRECATE	// Allows to use the unsecure fopen command
#include <iostream>
#include <memory.h>
#include <stdio.h>
#include <math.h>


// Define the type "RGBE"
typedef unsigned char RGBE[4];
#define R			0
#define G			1
#define B			2
#define E			3

// Define the scanline range
#define  MINELEN	8				// minimum scanline length for encoding
#define  MAXELEN	0x7fff			// maximum scanline length for encoding


/**
* Load HDR image and convert to 
* a set of float32 RGB triplet.
*/
class HDRImage
{
	// Image width and height
	int width, height;

	// The image data, each pixel takes three floats
	float *cols;

	// Converts a value based on the given exponent to a float
	static float convertComponent(int expo, int val);

	// Converts the values of a given RGBE vector to RGB
	static void workOnRGBE(RGBE* scan, int len, float* cols);

	// Reads the values of a given HDR file line and stores them in a given vector
	static bool decrunch(RGBE* scanline, int len, FILE* file);

	// Reads the values of a given HDR file line and stores them in a given vector
	static bool oldDecrunch(RGBE* scanline, int len, FILE* file);

public:
	// The constructor
	HDRImage(const char* fileName);

	// The destructor
	~HDRImage();

	// Whether there is data stored
	bool data;

	// Return a certain pixel
	float operator[](int i);

	// Return the height of the image
	int getHeight();

	// Return the width of the image
	int getWidth();
};


#endif