//////////////////////////////////////////////////////////////////////////////////////////////////
//
// Local Laplacian Filters: Edge-aware Image Processing with a Laplacian Pyramid
// For "Praktikum: Bildverarbeitung / Bildkommunikation"
// At the Eberhardt Karls Universität Tübingen, Germany
// 
// This project tries to implement a user friendly GUI to perform image processing
// using the local laplacian filtering method proposed by Paris et al. in their Paper
// "Local Laplacian Filters: Edge-aware Image Processing with a Laplacian Pyramid" (2011).
// The project is using the Gtk+ 3.6.4 GUI Library Bundle for the graphical interface and
// the OpenCV 2.4.9 Open Source Computer Vision Library Bundle for the computation. 
// The code for the HDRImage class is based on Igor Kravtchenko's code (flipcode.com).
//
// Credit goes to Sylvain Paris, Samuel W.Hasinoff, Jan Kautz and Igor Kravtchenko
//
// Author is Adrian Czarkowski
// See me also on Bitbucket, where this project is hosted:
// https://bitbucket.org/kinecticats/local-laplacian-filters-edge-aware-image-processing-with-a
//
//////////////////////////////////////////////////////////////////////////////////////////////////


#ifndef GUI_H 
#define GUI_H 

#include <glib-2.0\glib.h>
#include <gtk-3.0\gtk\gtk.h>
#include <opencv2\highgui\highgui.hpp>
#include "lapfilter.h"
#include "HDRImage.h"


/**
* GUI Class:
* Creates and manages the GUI
*/
class GUI
{
	// The main window
	GtkWidget* window;

	// The boxes to hold all widgets
	GtkWidget* main_box;				// Wrapper for all other boxes
	GtkWidget* top_box;					// Contains everything, expect the progressbar
	GtkWidget* left_box;				// Consists of the input image, the load buttons and the sliders
	GtkWidget* right_box;				// Consists of the output image, the other buttons and the radiobuttons
	GtkWidget* left_image;				// Contains the input image
	GtkWidget* right_image;				// Contains the output image
	GtkWidget* input_frame;				// The frame for the input image
	GtkWidget* output_frame;			// The frame for the output image
	GtkWidget* left_buttons;			// The load buttons
	GtkWidget* right_buttons;			// The other buttons
	GtkWidget* vbox_slider;				// Holds all sliders:
	GtkWidget* alpha;					// The box for the alpha slider
	GtkWidget* alpha_help;				// The help button box for alpha
	GtkWidget* beta;					// The box for the beta slider
	GtkWidget* beta_help;				// The help button box for beta
	GtkWidget* sigma;					// The box for the sigma slider
	GtkWidget* sigma_help;				// The help button box for sigma
	GtkWidget* noise;					// The box for the noise slider
	GtkWidget* noise_help;				// The help button box for noise
	GtkWidget* hbox_radios;				// padding
	GtkWidget* filler_radios;			// padding
	GtkWidget* vbox_radios;				// Holds the radio buttons:
	GtkWidget* vbox_radios_top;			// The pixel value processing radios
	GtkWidget* vbox_radios_bot;			// The color value processing radios
	GtkWidget* progressbar;				// The progressbar

	// The OpenCV images
	cv::Mat cv_input_image;
	cv::Mat cv_output_image;

	// The images
	GtkWidget* input_image;
	GtkWidget* output_image;

	// The radio button listeners
	GtkWidget* radio_top;
	GtkWidget* radio_bot;

	// Seperators
	GtkWidget* left_separator;
	GtkWidget* right_separator;
	GtkWidget* radio_separator;
	GtkWidget* image_separator;

	// The buttons
	GtkWidget* button_LDR;
	GtkWidget* button_HDR;
	GtkWidget* button_Start;
	GtkWidget* button_Save;

	// The button handler IDs
	gulong ldr_id;
	gulong hdr_id;
	gulong process_id;
	gulong save_id;
	gulong cancel_id;
	gulong alpha_id;
	gulong beta_id;
	gulong sigma_id;
	gulong noise_id;

	// The sliders and their help buttons
	GtkWidget* alpha_slider;	GtkWidget* alpha_button;
	GtkWidget* beta_slider;		GtkWidget* beta_button;
	GtkWidget* sigma_slider;	GtkWidget* sigma_button;
	GtkWidget* noise_slider;	GtkWidget* noise_button;

	// The radio buttons
	GtkWidget* linear_radio;
	GtkWidget* logarithmic_radio;
	GtkWidget* color_radio;
	GtkWidget* luminance_radio;

	// The progressbar
	GtkWidget* progress;

	// The progress in percent
	float percent;

	// Whether the processing should be stopped
	bool process;

	// Whether there are images loaded
	bool input;
	bool output;

	// The screen properties
	int width;
	int height;

public:
	// The constructor
	GUI();

	// The destructor
	~GUI();

	// Load a LDR image and display it
	static void loadLDR(GtkWidget* widget, GUI* gui);

	// Load a HDR image and display it
	static void loadHDR(GtkWidget* widget, GUI* gui);

	// Process the input image 
	static gboolean startProcessing(GtkWidget* widget, GUI* gui);

	// Save the output image
	static void saveResult(GtkWidget* widget, GUI* gui);

	// Update the percent value of the progressbar
	static gboolean updateProgressBar(GUI* gui);

	// Stop the processing
	static void stopProcessing(GtkWidget* widget, GUI* gui);

	// Show info message
	static gboolean showInfo(GUI* gui, const gchar* text);

	// Reset the GUI's image state after loading error
	static gboolean loadError(GUI* gui);

	// Reset the GUI after processing
	static gboolean processReset(GUI* gui);

	// Output the help for alpha
	static gboolean alphaHelp(GtkWidget* widget, GUI* gui);

	// Output the help for beta
	static gboolean betaHelp(GtkWidget* widget, GUI* gui);

	// Output the help for sigma
	static gboolean sigmaHelp(GtkWidget* widget, GUI* gui);

	// Output the help for noise
	static gboolean noiseHelp(GtkWidget* widget, GUI* gui);
};


#endif