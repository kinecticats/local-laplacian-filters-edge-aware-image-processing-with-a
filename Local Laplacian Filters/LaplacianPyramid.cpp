//////////////////////////////////////////////////////////////////////////////////////////////////
//
// Local Laplacian Filters: Edge-aware Image Processing with a Laplacian Pyramid
// For "Praktikum: Bildverarbeitung / Bildkommunikation"
// At the Eberhardt Karls Universität Tübingen, Germany
// 
// This project tries to implement a user friendly GUI to perform image processing
// using the local laplacian filtering method proposed by Paris et al. in their Paper
// "Local Laplacian Filters: Edge-aware Image Processing with a Laplacian Pyramid" (2011).
// The project is using the Gtk+ 3.6.4 GUI Library Bundle for the graphical interface and
// the OpenCV 2.4.9 Open Source Computer Vision Library Bundle for the computation. 
// The code for the HDRImage class is based on Igor Kravtchenko's code (flipcode.com).
//
// Credit goes to Sylvain Paris, Samuel W.Hasinoff, Jan Kautz and Igor Kravtchenko
//
// Author is Adrian Czarkowski
// See me also on Bitbucket, where this project is hosted:
// https://bitbucket.org/kinecticats/local-laplacian-filters-edge-aware-image-processing-with-a
//
//////////////////////////////////////////////////////////////////////////////////////////////////


#include "LaplacianPyramid.h"


/**
* Constructor for the laplacian pyramid
* @param image: the input image
* @param limit: the maximum depth
* @param subwindow: the subwindow
*/
LaplacianPyramid::LaplacianPyramid(cv::Mat image, int limit, cv::Vec4i subwindow)
{
	// Store the limit as depth
	levels = limit;

	// Create working variables
	cv::Mat J; cv::Vec4i subwindow_child;

	// Copy the input to the working variable
	image.copyTo(J);

	// Get the pyramid filter
	cv::Mat filter = get_filter();

	for (int i = 0; i < limit; i++)
	{
		// Apply low pass filter and sample
		image = downsample(J, filter, subwindow);

		// Create the child window for given subwindow
		subwindow_child = child_window(subwindow, 1);

		// Store difference between image and upsampled low pass version
		level.push_back(J - upsample(image, filter, subwindow));

		// Continue with the low pass image
		J = image;
		subwindow = subwindow_child;
	}
	// The coarest level contains the residual low pass image
	level.push_back(J);
}


/**
* Destructor for the laplacian pyramid
*/
LaplacianPyramid::~LaplacianPyramid()
{
	for (int i = 0; i < levels; i++)
	{
		level[i].release();
	}
}


/**
* Return a certain level
* @param i: the index
* @return The level at given index
*/
cv::Mat LaplacianPyramid::operator[](int i)
{
	// Check for valid index
	if (i < 0 || i >= levels)
	{
		throw std::out_of_range("Index is out of bounds");
	}
	else
	{
		return level[i];
	}
}


/**
* Return the depth of the pyramid
* @return The depth
*/
int LaplacianPyramid::depth()
{
	return levels;
}