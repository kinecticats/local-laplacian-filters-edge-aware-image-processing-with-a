//////////////////////////////////////////////////////////////////////////////////////////////////
//
// Local Laplacian Filters: Edge-aware Image Processing with a Laplacian Pyramid
// For "Praktikum: Bildverarbeitung / Bildkommunikation"
// At the Eberhardt Karls Universität Tübingen, Germany
// 
// This project tries to implement a user friendly GUI to perform image processing
// using the local laplacian filtering method proposed by Paris et al. in their Paper
// "Local Laplacian Filters: Edge-aware Image Processing with a Laplacian Pyramid" (2011).
// The project is using the Gtk+ 3.6.4 GUI Library Bundle for the graphical interface and
// the OpenCV 2.4.9 Open Source Computer Vision Library Bundle for the computation. 
// The code for the HDRImage class is based on Igor Kravtchenko's code (flipcode.com).
//
// Credit goes to Sylvain Paris, Samuel W.Hasinoff, Jan Kautz and Igor Kravtchenko
//
// Author is Adrian Czarkowski
// See me also on Bitbucket, where this project is hosted:
// https://bitbucket.org/kinecticats/local-laplacian-filters-edge-aware-image-processing-with-a
//
//////////////////////////////////////////////////////////////////////////////////////////////////


#ifndef LAPFILTER_CORE_H 
#define LAPFILTER_CORE_H

#include <limits.h>
#include "GaussianPyramid.h"
#include "LaplacianPyramid.h"


/**
* Convert an RGB image to grayscale intensity
* @param image: the input image
* return The grayscale image
*/
cv::Mat luminance(cv::Mat image);


/**
* Smooth step edge between (xmin,0) and (xmax,1)
* @param xmin: the minimum smoothing
* @param xmax: the maximum smoothing
* @param image: the matrix to smooth
* return The smoothed matrix
*/
cv::Mat smooth_step(float xmin, float xmax, cv::Mat image);


/**
* Alters the edges of the given matrix,
* used as detail remapping function
* @param image: the given matrix
* @param alpha: the edge factor
* @param noise: the noise level
* @param sigma_r: the edge sensitivity
* return The altered matrix
*/
cv::Mat fd(cv::Mat image, float alpha, float noise, float sigma_r);


/**
* Scales a matrix by beta,
* used as edge remapping function
* @param image: the matrix to scale
* @param beta: the scaling factor
* return The scaled matrix
*/
cv::Mat fe(cv::Mat image, float beta);


/**
* Remapping function which performs
* filtering on the input image with
* the given rgb pixel
* @param image: the input image
* @param rgb: one pixel of image
* @param alpha: the edge factor
* @param beta: the scaling factor
* @param sigma_r: the edge sensitivity
* @param noise: the noise level
* return The remapped image
*/
cv::Mat r_color(cv::Mat image, cv::Mat rgb, float alpha, float beta, float sigma_r, float noise);


/**
* Remapping function which performs
* filtering on the input image with
* the given gray pixel
* @param image: the input image
* @param gray: one pixel value
* @param alpha: the edge factor
* @param beta: the scaling factor
* @param sigma_r: the edge sensitivity
* @param noise: the noise level
* return The remapped image
*/
cv::Mat r_gray(cv::Mat image, float gray, float alpha, float beta, float sigma_r, float noise);


/**
* Reconstruct an image from a given laplace pyramid
* @param L: the given laplace pyramid
* return The reconstructed image
*/
cv::Mat reconstruct(LaplacianPyramid L);


/**
* Core functionality which performs
* the laplacian filtering for the
* wrapper 'lapfilter.h'
* @param image: the input image
* @param color: the remapping function flag
* @param alpha: the edge factor
* @param beta: the scaling factor
* @param sigma_r: the edge sensitivity
* @param noise: the noise level
* return The laplace-filtered image
*/
cv::Mat lapfilter_core(cv::Mat image, bool color, float alpha, float beta, float sigma_r, float noise);


#endif