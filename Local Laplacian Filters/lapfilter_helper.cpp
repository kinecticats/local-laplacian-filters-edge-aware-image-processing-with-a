//////////////////////////////////////////////////////////////////////////////////////////////////
//
// Local Laplacian Filters: Edge-aware Image Processing with a Laplacian Pyramid
// For "Praktikum: Bildverarbeitung / Bildkommunikation"
// At the Eberhardt Karls Universität Tübingen, Germany
// 
// This project tries to implement a user friendly GUI to perform image processing
// using the local laplacian filtering method proposed by Paris et al. in their Paper
// "Local Laplacian Filters: Edge-aware Image Processing with a Laplacian Pyramid" (2011).
// The project is using the Gtk+ 3.6.4 GUI Library Bundle for the graphical interface and
// the OpenCV 2.4.9 Open Source Computer Vision Library Bundle for the computation. 
// The code for the HDRImage class is based on Igor Kravtchenko's code (flipcode.com).
//
// Credit goes to Sylvain Paris, Samuel W.Hasinoff, Jan Kautz and Igor Kravtchenko
//
// Author is Adrian Czarkowski
// See me also on Bitbucket, where this project is hosted:
// https://bitbucket.org/kinecticats/local-laplacian-filters-edge-aware-image-processing-with-a
//
//////////////////////////////////////////////////////////////////////////////////////////////////


#include "lapfilter_helper.h"


void print3DMat(cv::Mat image, char* name)
{
	// Check the number of channels
	if (image.channels() == 3)
	{
		// Create array of three matrices
		cv::Mat channel[3];

		// Split the input matrix and fill the array
		cv::split(image, channel);

		// Output each channel seperatly
		std::cout << name << "(:,:,1) = \n\n" << channel[2] << std::endl << std::endl;
		std::cout << name << "(:,:,2) = \n\n" << channel[1] << std::endl << std::endl;
		std::cout << name << "(:,:,3) = \n\n" << channel[0] << std::endl << std::endl;
	}
	else
	{
		// Let the build in functions output the matrix
		std::cout << "The given matrix '" << name << "' is no RGB image!" << std::endl << std::endl;
		std::cout << image << std::endl << std::endl;
	}
}


cv::Mat make3DMat(cv::Mat matrix)
{
	// Check the number of channels
	if (matrix.channels() == 1)
	{
		// Create array of three matrices
		cv::Mat channel[3];

		// Initialize the output
		cv::Mat image_3D;

		// Distribute the input matrix
		channel[0] = matrix;
		channel[1] = matrix;
		channel[2] = matrix;

		// Merge the three channels
		merge(channel, 3, image_3D);

		// Return the 3D-matrix
		return image_3D;
	}
	else
	{
		// Return the given matrix unaltered
		std::cout << "The given matrix is not flat!" << std::endl << std::endl;
		return matrix;
	}
}


int numLevels(cv::Mat image)
{
	// Get the minimal dimension
	int min_d = cv::min(image.rows, image.cols);

	// Initialize the output
	int nlev = 1;

	// While there are levels bigger than 1x1 ...
	while (min_d > 1)
	{
		// ... Increment the depth
		nlev = nlev + 1;
		// Half the size of the current level
		min_d = floor((min_d + 1) / 2);
	}
	// Return the depth
	return nlev;
}


cv::Mat get_filter()
{
	// Initialize the working variables
	cv::Mat filter = (cv::Mat_<float>(1, 5) << 0.05, 0.25, 0.4, 0.25, 0.05);

	// Multiplicate with the transposed to create a 2D matrix
	return (filter.t() * filter);
}


cv::Vec4i child_window(cv::Vec4i parent, int N)
{
	// Initialize the working variables
	cv::Vec4f temp = parent;
	cv::Vec4i child;

	// Iterate for N pyramid levels
	for (int L = 0; L < N; L++)
	{
		// Iterate over the subwindow indices
		for (int i = 0; i < 4; i++)
		{
			// Downsample to imitate a lower pyramid level
			temp[i] = (temp[i] + 1) / 2;

			// Perform 'ceil' on the upper indices
			if (i == 0 || i == 2)
			{
				temp[i] = ceil(temp[i]);
			}
			// Perform 'floor' on the lower indices
			if (i == 1 || i == 3)
			{
				temp[i] = floor(temp[i]);
			}
		}
	}
	// Fill the result with the computed values
	for (int i = 0; i < 4; i++)
	{
		child[i] = temp[i];
	}
	// Return the result
	return child;
}


cv::Mat percentile(cv::Mat matrix, float prc)
{
	// Initialize the working variables
	cv::Mat result = cv::Mat_<float>(1, matrix.cols);
	int rank; float percentile;

	// Sort the matrix column-wise in ascending order
	cv::sort(matrix, matrix, CV_SORT_EVERY_COLUMN + CV_SORT_ASCENDING);

	// Compute the rank
	if (prc >= 90)
	{
		rank = ceil((prc / 100) * matrix.rows - 0.5);

		// Initialize the result
		result.at<float>(0, 0) = matrix.at<float>(matrix.rows-1, 0);
	}
	else
	{
		rank = ceil((prc / 100) * matrix.rows - 0.5);

		// Initialize the result
		result.at<float>(0, 0) = matrix.at<float>(rank, 0) + (matrix.rows * ((prc - ((rank + 0.5) / matrix.rows) * 100) / 100)) * (matrix.at<float>(rank + 1, 0) - matrix.at<float>(rank,0));
	}
	// Compute the percentile of the next lower index
	float prev_prc = ((rank + 0.5)/matrix.rows) * 100;

	// Iterate over every column
	for (int x = 1; x < matrix.cols; x++)
	{
		// Perform a linear interpolation between closest ranks
		percentile = matrix.at<float>(rank, x) + (matrix.rows * ((prc - prev_prc) / 100)) * (matrix.at<float>(rank + 1, x) - matrix.at<float>(rank, x));

		// Add the percentile to the result
		result.at<float>(0, x) = percentile;
	}
	// Return the result
	return result;
}


cv::Mat imfilter(cv::Mat image, cv::Mat kernel)
{
	// Initialize the result
	cv::Mat result;

	// Create the working variables
	cv::Point anchor(-1, -1);
	double delta = 0;

	// Check the number of channels
	if (image.channels() == 3)
	{
		// Create matrix array to split the image
		cv::Mat channel[3];

		// Split the image into its channels
		cv::split(image, channel);

		// Apply the filter on every channel
		cv::filter2D(channel[0], channel[0], -1, kernel, anchor, delta, cv::BORDER_CONSTANT);
		cv::filter2D(channel[1], channel[1], -1, kernel, anchor, delta, cv::BORDER_CONSTANT);
		cv::filter2D(channel[2], channel[2], -1, kernel, anchor, delta, cv::BORDER_CONSTANT);

		// Merge the channel into one image
		cv::merge(channel, 3, result);

		// Return the convoluted image
		return result;
	}
	if (image.channels() == 1)
	{
		// Apply the filter on the flat matrix
		cv::filter2D(image, image, -1, kernel, anchor, delta, cv::BORDER_CONSTANT);

		// Return the convoluted image
		return image;
	}
	else
	{
		// Return the given matrix unaltered
		std::cout << "The given matrix is no image!" << std::endl << std::endl;
		return image;
	}
}


cv::Mat downsample(cv::Mat image, cv::Mat filter, cv::Vec4i subwindow)
{
	// Store the size of the input image
	int height = image.rows;
	int width = image.cols;

	// Initialize the working variables
	cv::Mat weight, temp;

	// Convolve with 2D filter
	temp = imfilter(image, filter);

	// Check the number of channels
	if (image.channels() == 3)
	{
		// Reweight the image
		weight = imfilter(make3DMat(cv::Mat::ones(height, width, CV_32FC1)), filter);
		temp = temp / weight;
	}
	if (image.channels() == 1)
	{
		// Reweight the image
		weight = imfilter(cv::Mat::ones(height, width, CV_32FC1), filter);
		temp = temp / weight;
	}
	// Check if the subwindow indices are even
	int y_even = (subwindow[0] % 2) == 0;
	int x_even = (subwindow[2] % 2) == 0;

	// Initialize the output matrix of half the size as the input matrix
	float half_height = height - y_even; half_height = half_height / 2;
	float half_width = width - x_even; half_width = half_width / 2;
	cv::Mat result = cv::Mat::zeros(ceil(half_height), ceil(half_width), image.type());

	// Downsample by discarding every second row and column
	for (float x = x_even; x < width; x += 2)
	{
		for (float y = y_even; y < height; y += 2)
		{
			// Check the number of channels
			if (image.channels() == 3)
			{
				result.at<cv::Vec3f>(floor(y / 2), floor(x / 2)) = temp.at<cv::Vec3f>(y, x);
			}
			if (image.channels() == 1)
			{
				result.at<float>(floor(y / 2), floor(x / 2)) = temp.at<float>(y, x);
			}
		}
	}
	// Return the downsampled image
	return result;
}


cv::Mat upsample(cv::Mat image, cv::Mat filter, cv::Vec4i subwindow)
{
	// Increase the size to match the dimension of the parent subwindow
	int height = subwindow[1] - subwindow[0] + 1;
	int width = subwindow[3] - subwindow[2] + 1;

	// Check if the subwindow indices are even
	int y_even = (subwindow[0] % 2) == 0;
	int x_even = (subwindow[2] % 2) == 0;

	// Initialize the working variables
	cv::Mat weight, result;
	cv::Vec3f one = { 1, 1, 1 };
	result = cv::Mat::zeros(height, width, image.type());
	weight = cv::Mat::zeros(height, width, image.type());

	// Upsample by copying every second row and column...
	for (float x = x_even; x < width; x += 2)
	{
		for (float y = y_even; y < height; y += 2)
		{
			// Check the number of channels
			if (image.channels() == 3)
			{
				result.at<cv::Vec3f>(y, x) = image.at<cv::Vec3f>(floor(y / 2), floor(x / 2));
			}
			if (image.channels() == 1)
			{
				result.at<float>(y, x) = image.at<float>(floor(y / 2), floor(x / 2));
			}
		}
	}
	//... and convolving with the 2D upsampling filter
	result = imfilter(result, filter);

	// Fill every second row and column in the weight matrix with ones...
	for (int x = x_even; x < width; x += 2)
	{
		for (int y = y_even; y < height; y += 2)
		{
			// Check the number of channels
			if (image.channels() == 3)
			{
				weight.at<cv::Vec3f>(y, x) = one;
			}
			if (image.channels() == 1)
			{
				weight.at<float>(y, x) = 1;
			}
		}
	}
	//... and convolve with the 2D upsampling filter
	weight = imfilter(weight, filter);

	// Reweight the image
	result = result / weight;

	// Return the upsampled image
	return result;
}