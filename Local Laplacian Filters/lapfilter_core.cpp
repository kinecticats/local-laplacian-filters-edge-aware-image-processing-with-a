﻿//////////////////////////////////////////////////////////////////////////////////////////////////
//
// Local Laplacian Filters: Edge-aware Image Processing with a Laplacian Pyramid
// For "Praktikum: Bildverarbeitung / Bildkommunikation"
// At the Eberhardt Karls Universität Tübingen, Germany
// 
// This project tries to implement a user friendly GUI to perform image processing
// using the local laplacian filtering method proposed by Paris et al. in their Paper
// "Local Laplacian Filters: Edge-aware Image Processing with a Laplacian Pyramid" (2011).
// The project is using the Gtk+ 3.6.4 GUI Library Bundle for the graphical interface and
// the OpenCV 2.4.9 Open Source Computer Vision Library Bundle for the computation. 
// The code for the HDRImage class is based on Igor Kravtchenko's code (flipcode.com).
//
// Credit goes to Sylvain Paris, Samuel W.Hasinoff, Jan Kautz and Igor Kravtchenko
//
// Author is Adrian Czarkowski
// See me also on Bitbucket, where this project is hosted:
// https://bitbucket.org/kinecticats/local-laplacian-filters-edge-aware-image-processing-with-a
//
//////////////////////////////////////////////////////////////////////////////////////////////////


#include "lapfilter_core.h"


cv::Mat luminance(cv::Mat image)
{
	// Check the number of channels
	if (image.channels() == 3)
	{
		// Initialize the result
		cv::Mat result;

		// Create array of three matrices
		cv::Mat channel[3];

		// Split the input matrix
		cv::split(image, channel);

		// Create the grayscale image by adding all channels into one
		result = (channel[0] + (40 * channel[1]) + (20 * channel[2])) / 61;

		// Return the result
		return result;
	}
	if (image.channels() == 1)
	{
		// Return the image
		return image;
	}
	else
	{
		// Return the given image unaltered
		std::cout << "The given matrix is no image!" << std::endl << std::endl;
		return image;
	}
}


cv::Mat smooth_step(float xmin, float xmax, cv::Mat image)
{
	// Initialize the working variables
	cv::Mat tmp1, tmp2, result;

	// Divide by the middled edge
	cv::divide((image - xmin), (xmax - xmin), result);
	result = cv::max(0, cv::min(1, result));

	// Smooth step edge
	cv::pow(result, 2, tmp1);
	cv::pow((result - 2), 2, tmp2);
	cv::multiply(tmp1, tmp2, result);

	// Return the result
	return result;
}


cv::Mat fd(cv::Mat image, float alpha, float noise, float sigma_r)
{
	// Initialize the working variables
	cv::Mat tau, result;

	// Scale the matrix by alpha
	cv::pow(image, alpha, result);

	// Check whether edges should be enhanced
	if (alpha < 1)
	{
		// Get an image with smoothed step edge
		cv::Mat tau = smooth_step(noise, 2 * noise, image * sigma_r);

		// Amplify the edges using 'tau'
		cv::multiply((1 - tau), image, image);
		cv::multiply(tau, result, result);

		// Normalize the edges
		result = result + image;
	}
	// Return the result
	return result;
}


cv::Mat fe(cv::Mat image, float beta)
{
	// Scale the matrix by beta
	return (beta * image);
}


cv::Mat r_color(cv::Mat image, cv::Mat rgb, float alpha, float beta, float sigma_r, float noise)
{
	// Initialize the working variables
	cv::Mat tmp1, tmp2, dnrm, unit, g0, rd, re, isedge, result; cv::Mat channel[3];

	// Blow up g0 to the size of the input image
	g0 = cv::repeat(rgb, image.rows, image.cols);
	
	// Precalculate the detail factor 'dnrm'
	cv::pow((image - g0), 2, tmp1);
	
	// Sum the R, G, and B channel up to one matrix 
	cv::split(tmp1, channel);
	tmp1 = channel[0] + channel[1] + channel[2];
	tmp1.convertTo(tmp2, CV_32FC1);

	// Normalize the precalculation
	cv::sqrt(tmp2, dnrm);

	// Divide the image by 'dnrm' to get 'unit'
	tmp1 = image - g0;
	tmp2 = DBL_EPSILON + dnrm;
	tmp2 = make3DMat(tmp2);
	tmp1.convertTo(tmp1, CV_32FC3);
	cv::divide(tmp1, tmp2, unit);
	
	// Get the remapped detail 'rd' by altering the details using fd()...
	tmp1 = sigma_r * fd(dnrm / sigma_r, alpha, noise, sigma_r);
	tmp2 = make3DMat(tmp1);
	// ... and applying it to the image
	cv::multiply(unit, tmp2, tmp1);
	g0.convertTo(g0, CV_32FC3);
	rd = g0 + tmp1;

	// Get the remapped edge 're' by scaling the edges using fe()...
	tmp1 = fe(dnrm - sigma_r, beta) + sigma_r;
	tmp2 = make3DMat(tmp1);
	// ... and applying it to the image
	cv::multiply(unit, tmp2, tmp1);
	re = g0 + tmp1;
	
	// Keep details greater than the edge sensitivity 'sigma_r'
	tmp1 = dnrm > sigma_r;
	// Make a boolean matrix out of it
	tmp1 = tmp1 / 255;
	// Turn it into a 3D matrix holding the edges
	isedge = make3DMat(tmp1);
	
	// Edge-Detail separation based on 'sigma_r'
	tmp2 = isedge * 255;
	cv::bitwise_not(tmp2, tmp1);
	tmp1 = tmp1 / 255;
	tmp1.convertTo(tmp1, CV_32FC3);
	cv::multiply(tmp1, rd, tmp2);
	isedge.convertTo(isedge, CV_32FC3);
	cv::multiply(isedge, re, tmp1);
	result = tmp1 + tmp2;

	// Return the result
	return result;
}


cv::Mat r_gray(cv::Mat image, float gray, float alpha, float beta, float sigma_r, float noise)
{
	// Initialize the working variables
	cv::Mat tmp1, tmp2, dnrm, dsgn, rd, re, isedge, result;

	// Precalculate difference
	tmp1 = (image - gray);

	// Calculate the factors
	dnrm = cv::abs(tmp1);
	dsgn = tmp1 / dnrm;

	// Get the remapped detail 'rd' by altering the details using fd()
	tmp1 = fd(dnrm / sigma_r, alpha, noise, sigma_r);
	tmp2 = dsgn * sigma_r;
	cv::multiply(tmp1, tmp2, rd);
	rd = rd + gray;

	// Get the remapped edge 're' by scaling the edges using fe()
	tmp1 = fe(dnrm - sigma_r, beta) + sigma_r;
	cv::multiply(tmp1, dsgn, re);
	re = re + gray;

	// Edge-Detail separation based on 'sigma_r'
	isedge = dnrm > sigma_r;
	cv::bitwise_not(isedge, tmp1);
	tmp1 = tmp1 / 255; isedge = isedge / 255;
	tmp1.convertTo(tmp1, CV_32FC1);
	cv::multiply(tmp1, rd, tmp2);
	isedge.convertTo(isedge, CV_32FC1);
	cv::multiply(isedge, re, tmp1);
	result = tmp1 + tmp2;

	// Return the result
	return result;
}


cv::Mat reconstruct(LaplacianPyramid L)
{
	// Store the size of the first pyramid level
	int height = L[0].rows;
	int width = L[0].cols;

	// Store the depth of the pyramid
	int depth = L.depth();

	// Initialize the working variables
	cv::Mat subwindow_all, filter, up, result;
	cv::Vec4i subwindow = { 1, height, 1, width };
	subwindow_all = cv::Mat::zeros(depth - 1, 4, CV_32SC1);

	// Initialize the subwindow matrix
	subwindow_all.at<int>(0, 0) = subwindow[0];
	subwindow_all.at<int>(0, 1) = subwindow[1];
	subwindow_all.at<int>(0, 2) = subwindow[2];
	subwindow_all.at<int>(0, 3) = subwindow[3];

	for (int l = 1; l < depth - 1; l++)
	{
		// Construct child window for the last subwindow
		subwindow = child_window(subwindow_all.row(l-1), 1);

		// Fill the current row with the child window
		subwindow_all.at<int>(l, 0) = subwindow[0];
		subwindow_all.at<int>(l, 1) = subwindow[1];
		subwindow_all.at<int>(l, 2) = subwindow[2];
		subwindow_all.at<int>(l, 3) = subwindow[3];
	}
	// Start with low pass residual
	result = L[depth - 1];

	// Get the pyramid filter
	filter = get_filter();

	// Iterate over each pyramid level
	for (int i = depth - 2; i >= 0; i--)
	{
		// Reconstruct the next level
		result = L[i] + upsample(result, filter, subwindow_all.row(i));
	}
	// Return the reconstructed image
	return result;
}


cv::Mat lapfilter_core(cv::Mat image, bool color, float alpha, float beta, float sigma_r, float noise)
{
	// Store the size of the first pyramid level
	int height = image.rows;
	int width = image.cols;

	// Create the initial subwindow
	cv::Vec4i subwindow = { 1, height, 1, width };

	// Create the gaussian pyramid
	GaussianPyramid GP(image, numLevels(image));

	// Allocate memory for the laplacian pyramid
	LaplacianPyramid LP(cv::Mat::zeros(image.size(), image.type()), numLevels(image), subwindow);

	// Initialize the working variables
	cv::Mat Iremap, Isub; cv::Range xrng, yrng;
	int hw, xf, yf, xfc, yfc; float gray, xfclevel, yfclevel;
	float level_factor, row_factor, percent;
	cv::Mat_<cv::Vec3f> rgb(1, 1, cv::Vec3f(0, 0, 0));

	// Output textual information
	std::cout << std::endl << "+-----------------+";
	std::cout << std::endl << "| Start filtering |";
	std::cout << std::endl << "+-----------------+" << std::endl << std::endl;

	// Perform the efficent laplacian filtering with O(N log N)
	for (int level = 0; level < LP.depth() - 1; level++)
	{
		for (int y = 0; y < GP[level].rows; y++)
		{
			// Output the progress
			level_factor = level; level_factor = level_factor / (LP.depth() - 1);
			row_factor = y; row_factor = row_factor / GP[level].rows; row_factor = row_factor / (LP.depth() - 1);
			percent = level_factor + row_factor;
			std::cout << "Processing level " << level + 1 << " of " << LP.depth() - 1 << "\t (" << percent * 100 << "%)" << std::endl;

			for (int x = 0; x < GP[level].cols; x++)
			{
				// Calculate half-width of full-res footprint
				hw = 3 * pow(2, level + 1) - 2;

				// Coords in full-res image corresponding to [level, y, x]
				xf = x * pow(2, level);
				yf = y * pow(2, level);

				// Subwindow in full-res image needed to evaluate [level, y, x) in result
				xrng = cv::Range(std::max(0, xf - hw), std::min(image.cols, xf + hw + 1));
				yrng = cv::Range(std::max(0, yf - hw), std::min(image.rows, yf + hw + 1));

				// Extracted subwindow from input image
				Isub = image(yrng, xrng);

				// Check which processing style
				if (color)
				{
					// Calculate the remapped subwindow
					rgb = GP[level].at<cv::Vec3f>(y, x); // g0 = [BGR]
					Iremap = r_color(Isub, rgb, alpha, beta, sigma_r, noise);
				}
				else
				{
					// Calculate the remapped subwindow
					gray = GP[level].at<float>(y, x); // g0 = [gray]
					Iremap = r_gray(Isub, gray, alpha, beta, sigma_r, noise);
				}
				// Distribute the subwindow indices over the subwindow vector
				subwindow[0] = (yrng.start + 1);
				subwindow[1] = yrng.end;
				subwindow[2] = (xrng.start + 1);
				subwindow[3] = xrng.end;

				// Compute Laplacian pyramid for the remapped subwindow
				LaplacianPyramid Lremap(Iremap, level + 2, subwindow);

				// Compute index of [level, y, x] within subwindow, at full-res and at current pyramid level
				xfc = xf - xrng.start; xfclevel = xfc / pow(2, level); xfclevel = floor(xfclevel);
				yfc = yf - yrng.start; yfclevel = yfc / pow(2, level); yfclevel = floor(yfclevel);

				// Check the number of channels
				if (image.channels() == 3)
				{
					// Set coefficient in result based on the corresponding coefficient in the remapped pyramid
					LP[level].at<cv::Vec3f>(y, x) = Lremap[level].at<cv::Vec3f>(yfclevel, xfclevel);
				}
				if (image.channels() == 1)
				{
					// Set coefficient in result based on the corresponding coefficient in the remapped pyramid
					LP[level].at<float>(y, x) = Lremap[level].at<float>(yfclevel, xfclevel);
				}
			}
		}
	}
	// Reset the progress value
	percent = 0.00;

	// Assign the lowest gauss pyramid level to the lowest laplace pyramid level
	GP[GP.depth() - 1].copyTo(LP[LP.depth() - 1]);

	// Reconstruct the image
	return reconstruct(LP);
}