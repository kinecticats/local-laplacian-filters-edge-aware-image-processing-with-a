//////////////////////////////////////////////////////////////////////////////////////////////////
//
// Local Laplacian Filters: Edge-aware Image Processing with a Laplacian Pyramid
// For "Praktikum: Bildverarbeitung / Bildkommunikation"
// At the Eberhardt Karls Universität Tübingen, Germany
// 
// This project tries to implement a user friendly GUI to perform image processing
// using the local laplacian filtering method proposed by Paris et al. in their Paper
// "Local Laplacian Filters: Edge-aware Image Processing with a Laplacian Pyramid" (2011).
// The project is using the Gtk+ 3.6.4 GUI Library Bundle for the graphical interface and
// the OpenCV 2.4.9 Open Source Computer Vision Library Bundle for the computation. 
// The code for the HDRImage class is based on Igor Kravtchenko's code (flipcode.com).
//
// Credit goes to Sylvain Paris, Samuel W.Hasinoff, Jan Kautz and Igor Kravtchenko
//
// Author is Adrian Czarkowski
// See me also on Bitbucket, where this project is hosted:
// https://bitbucket.org/kinecticats/local-laplacian-filters-edge-aware-image-processing-with-a
//
//////////////////////////////////////////////////////////////////////////////////////////////////


#include "lapfilter.h"


cv::Mat lapfilter(cv::Mat image, bool color, bool logarithmic, float alpha, float beta, float sigma_r, float noise)
{
	// Initilialize the working variables
	cv::Mat image_lum, result_lum, lum_ratio, result;

	// Adjust parameters if not linear 
	if (logarithmic)
	{
		sigma_r = log(sigma_r);
	}
	// Adjust parameters if not in color
	if (!color)
	{
		image_lum = luminance(image);
		cv::divide(image, make3DMat(image_lum + DBL_EPSILON), lum_ratio);
		image_lum.copyTo(image);
	}
	// Check if processing is needed
	if (alpha == 1 && beta == 1)
	{
		image.copyTo(result);
	}
	else
	{
		// Preprocess the input if logarithmic
		if (logarithmic)
		{
			// Convert the image to the logarithmic domain
			cv::log(image + DBL_EPSILON, image);
		}
		// Perform the laplacian filtering
		result = lapfilter_core(image, color, alpha, beta, sigma_r, noise);

		// Postprocess the result if logarithmic
		if (logarithmic)
		{
			cv::exp(result, result);
			result = result - DBL_EPSILON;
		}
	}
	// General postprocessing
	if (logarithmic && beta <= 1)
	{
		// For tone mapping, remap middle 99% of intensities
		// to fixed dynamic range using a gamma curve
		int dyn_rng = 100;
		float prc_clip = 0.5;
		result_lum = luminance(result);
		result_lum = (result_lum.reshape(1, 1)).t();
		float result_max_clip = percentile(result_lum, 100 - prc_clip).at<float>(0, 0);
		float result_min_clip = percentile(result_lum, prc_clip).at<float>(0, 0);
		float dyn_rng_clip = result_max_clip / result_min_clip;
		float exponent = log(dyn_rng) / log(dyn_rng_clip);
		result = cv::max(0, result / result_max_clip);
		cv::pow(result, exponent, result);
	}
	if (!color)
	{
		// Reintroduce color ratios
		cv::multiply(make3DMat(result), lum_ratio, result);
	}
	// Clip out of bounds intensities
	result = cv::max(0, result);
	if (beta <= 1)
	{
		result = cv::min(1, result);
	}
	// For tone mapping, gamma correct linear intensities for display
	if (logarithmic && beta <= 1)
	{
		float gamma = 2.2;
		cv::pow(result, 1 / gamma, result);
	}
	// Output the result
	return result;
}