//////////////////////////////////////////////////////////////////////////////////////////////////
//
// Local Laplacian Filters: Edge-aware Image Processing with a Laplacian Pyramid
// For "Praktikum: Bildverarbeitung / Bildkommunikation"
// At the Eberhardt Karls Universität Tübingen, Germany
// 
// This project tries to implement a user friendly GUI to perform image processing
// using the local laplacian filtering method proposed by Paris et al. in their Paper
// "Local Laplacian Filters: Edge-aware Image Processing with a Laplacian Pyramid" (2011).
// The project is using the Gtk+ 3.6.4 GUI Library Bundle for the graphical interface and
// the OpenCV 2.4.9 Open Source Computer Vision Library Bundle for the computation. 
// The code for the HDRImage class is based on Igor Kravtchenko's code (flipcode.com).
//
// Credit goes to Sylvain Paris, Samuel W.Hasinoff, Jan Kautz and Igor Kravtchenko
//
// Author is Adrian Czarkowski
// See me also on Bitbucket, where this project is hosted:
// https://bitbucket.org/kinecticats/local-laplacian-filters-edge-aware-image-processing-with-a
//
//////////////////////////////////////////////////////////////////////////////////////////////////


#ifndef GAUSSIANPYRAMID_H 
#define GAUSSIANPYRAMID_H 

#include "lapfilter_helper.h"


/**
* Gaussian Pyramid Class:
* Creates a gaussian pyramid
* for a given input image
*/
class GaussianPyramid
{
	// A vector of matrices to hold the pyramid levels
	cv::vector<cv::Mat> level;

	// The depth of the pyramid
	int levels;

public:
	// The constructor
	GaussianPyramid(cv::Mat image, int limit);

	// The destructor
	~GaussianPyramid();

	// Return a certain level
	cv::Mat operator[](int i);

	// Return the depth of the pyramid
	int depth();
};


#endif