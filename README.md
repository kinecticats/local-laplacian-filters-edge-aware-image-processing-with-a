# Readme #

### 1. Preamble ###

	For "Praktikum: Bildverarbeitung / Bildkommunikation"
	At the Eberhardt Karls Universitaet Tuebingen, Germany

	This project tried to implement a user friendly GUI to perform image processing
	using the local laplacian filtering method proposed by Paris et al. in their Paper
	"Local Laplacian Filters: Edge-aware Image Processing with a Laplacian Pyramid" (2011).


### 2. Compilation ###

	To compile the project download the code.
	You will need to install:

		- The C/C++ GUI Library Bundle Gtk+ (3.6.4)
          which consists of a broad amount of GNU licensed standalone Software
		  -> http://www.gtk.org/


		- The C/C++ Computer Vision Library Bundle OpenCV (2.4.9)
          which is a BSD licensed standalone Software
		  -> http://opencv.org/



### 3. Author ###

	Author are Adrian Czarkowski and Holger Honz



### 4. Credits ###

	Credit goes to Sylvain Paris, Samuel W.Hasinoff and Jan Kautz for their paper,
	to Igor Kravtchenko for his used code (flipcode.com) to provide the HDRImage class
	and of course to all the provided libraries.



### 5. Sources ###

	The following sources were used:
	- Gtk+
	- OpenCV